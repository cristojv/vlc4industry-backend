from occcamera.occreceiver import OCCReceiver
from multiprocessing import SimpleQueue
from events.cameraevents import CameraEvents, CameraEvent
from events.coordinatorevents import CoordinatorEvents, CoordinatorEvent
import time
if __name__ == "__main__":
    inputQueue = SimpleQueue()
    outputQueue = SimpleQueue()
    occreceiver = OCCReceiver(inputQueue,outputQueue)
    occreceiver.start()
    inputQueue.put(CameraEvent(CameraEvents.CAMERA_LAUNCH))
    time.sleep(1)
    