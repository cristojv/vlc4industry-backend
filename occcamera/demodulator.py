# import the necessary packages

import numpy as np
import cv2
import matplotlib.pyplot as plt
#import binascii

class Demodulator:
    def __init__(self):
        self._currentID = -1;
        self._maxCurrentID = 4;
        pass
    def enhanceSignal(self, signal, invColorMatrix, invSpatialMatrix):
        status = 0
        enhancedSignal = 0;
        numberOfSamples = signal.shape[0];
        colorAdaptedSignal = [];
        for i in range (0,numberOfSamples):
            colorAdaptedSample = np.dot(signal[i],invColorMatrix[i]).reshape(1, 3)
            # colorAdaptedSample = np.matmul(invColorMatrix[i],signal[i]).reshape(1, 3)
            if i == 0:
                colorAdaptedSignal = colorAdaptedSample
            else:
                colorAdaptedSignal = np.concatenate((colorAdaptedSignal,colorAdaptedSample))
        spatialAdaptedSignal = np.multiply(colorAdaptedSignal,invSpatialMatrix);

        enhancedSignal = spatialAdaptedSignal;
        
        #enhancedSignal = colorAdaptedSignal;
        # enhancedSignal = colorAdaptedSignal
        # enhancedSignal[:,0] = enhancedSignal[:,0] * (255/ np.max(enhancedSignal[:,0]))
        # enhancedSignal[:,1] = enhancedSignal[:,1] * (255/ np.max(enhancedSignal[:,1]))
        # enhancedSignal[:,2] = enhancedSinal[:,2] * (255/ np.max(enhancedSignal[:,2]))
        #green = bytescale(enhancedSignal[:,1]).reshape(numberOfSamples,1)
        #blue = bytescale(enhancedSignal[:,0]).reshape(numberOfSamples,1)
        #red = bytescale(enhancedSignal[:,2]).reshape(numberOfSamples,1)
        #enhancedSignal = np.concatenate((blue,green,red), axis=1)
        #enhancedSignalE = enhancedSignal.copy()
        #Expand signal
        #enhancedSignalE = cv2.normalize(enhancedSignalE,None,0,1,norm_type=cv2.NORM_MINMAX,dtype=cv2.CV_32F)
        #enhancedSignalE = np.array(enhancedSignalE)*255
        #enhancedSignalE.astype(np.uint8)
        #figure = plt.figure();
        #graph = figure.add_subplot(311)
        #graph1 = figure.add_subplot(312)
        #graph2 = figure.add_subplot(313)
        #plt.ion();
        #graph.plot(enhancedSignal[:, 0], 'b')
        #graph.plot(enhancedSignal[:, 1], 'g')
        #graph.plot(enhancedSignal[:, 2], 'r')
        #graph1.plot(signal[:, 0], 'b')
        #graph1.plot(signal[:, 1], 'g')
        #graph1.plot(signal[:, 2], 'r')
        #plt.show()
        status = 1;
        return status, enhancedSignal;

    def binarizeSignal(self, signal, threshold=127, debug=0):
        # Return variables
        status = 0
        binarizedSignal = 0
        # Binarization
        binarizedSignal = np.copy(signal);
        numberOfSamples = binarizedSignal.shape[0];
        blue = binarizedSignal[:, 0]
        blue[blue < threshold] = 0
        blue[blue >= threshold] = 1
        green = binarizedSignal[:, 1]
        green[green < threshold] = 0;
        green[green >= threshold] = 1;
        red = binarizedSignal[:, 2]
        red[red < threshold] = 0;
        red[red >= threshold] = 1;
        binarizedSignal = np.concatenate(
            (blue.reshape(numberOfSamples, 1), green.reshape(numberOfSamples, 1), red.reshape(numberOfSamples, 1)), axis=1)

        status = 1;

        return status, binarizedSignal;

    def demodulateSignal(self, signal, thresholdLevel):
        status = 0
        numberOfSamples = signal.shape[0]//5
        #print("Signal shape> {}".format(signal.shape[0]))
        index = np.arange(numberOfSamples,signal.shape[0]+1,numberOfSamples)
        index = index - numberOfSamples//2
        
        threshold1_values = np.take(thresholdLevel[:,0], indices= index)
        threshold2_values = np.take(thresholdLevel[:,2], indices= index)
        channel1_values = np.take(signal[:,0], indices= index)
        channel2_values = np.take(signal[:,2], indices= index)
        
        channel1_values = np.less(threshold1_values,channel1_values).astype(np.uint8)
        channel2_values = np.less(threshold2_values,channel2_values).astype(np.uint8)
        #print(channel1_values)
        #print(channel2_values)
        #ID
        id = ''.join(str(e) for e in list((channel1_values[0],channel2_values[0])))
        id = int(id,2)
        #print("ID> {}".format(id))
        #Cases:
        # currentID == -1: ID not yet defined. Using ID from first packet received
        # return 1
        # currentID == id: Same ID discard packet
        # return 0
        # (currentID+1)%4 == id: Next ID, process packet
        # return 1
        # else: Error within ID, discard packet
        # return -1 (reset ID?)
        #channel1Data = channel1_values[1:]
        #channel2Data = channel2_values[1:]
        #byte = np.array(list(zip(channel1Data, channel2Data)))
        #byte = byte.reshape(byte.size).astype(np.uint8)
        #return 1,id,byte
        
        if(self._currentID==-1):
            #First time receiving data or recovering after an error
            
            channel1Data = channel1_values[1:]
            channel2Data = channel2_values[1:]
            byte = np.array(list(zip(channel1Data, channel2Data)))
            byte = byte.reshape(byte.size).astype(np.uint8)
            self._currentID = id;
            status = 1
            return status,id,byte
        
        elif(self._currentID==id):
            #Same packet so It's necesary discarting
            #channel1Data = channel1_values[1:]
            #channel2Data = channel2_values[1:]
            #byte = np.array(list(zip(channel1Data, channel2Data)))
            #byte = byte.reshape(byte.size).astype(np.uint8)
            self._currentID = id;
            status = 2
            return status,id,None
        
        elif(((self._currentID+1)%self._maxCurrentID==id)):
            #Next byte data
            channel1Data = channel1_values[1:]
            channel2Data = channel2_values[1:]
            byte = np.array(list(zip(channel1Data, channel2Data)))
            byte = byte.reshape(byte.size).astype(np.uint8)
            self._currentID = id;
            status = 1
            return status,id,byte
        
        else:
            #An error has arise. Can be due to an error within decoding
            #or due frame-droping
            self._currentID=-1
            #Data
            channel1Data = channel1_values[1:]
            channel2Data = channel2_values[1:]
            byte = np.array(list(zip(channel1Data, channel2Data)))
            byte = byte.reshape(byte.size).astype(np.uint8)
            status = 3
            return status,id,byte
        
    def int2bytes(self, i):
        hex_string = '%x' % i
        n = len(hex_string)
        return binascii.unhexlify(hex_string.zfill(n + (n & 1)))

    def convertByteToChar(self, bits, encoding='utf-8', errors='surrogatepass'):
        status = 0
        bits = ''.join(str(i) for i in bits)
        n = int(bits,2)
        #try:
         #   n.to_bytes(1,'big').decode(encoding,errors)
       # except:
        #    print("Error on decoding")
         #   n = none
        return status, n