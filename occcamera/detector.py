#import the necessary packages

import cv2
import numpy as np

class Detector:

    def __init__(self, rowHeight, numberOfRows, columnHeight, numberOfPackets):

        #self._isRoiDetected = False
        self._roi = np.array((0, 0, 0, 0))
        self._sroi = np.array((0, 0, 0, 0))
        self._packet = np.array((0, 0, 0, 0))

        # Templates
        self._rowHeight = rowHeight
        self._numberOfRows = numberOfRows
        self._columHeight = columnHeight
        self._numberOfPackets = numberOfPackets
        
        self._sourceTemplate = self.generateIdleTemplate(rowHeight=self._rowHeight, numberOfRows=self._numberOfRows, columnHeight=self._columHeight, forPackets=self._numberOfPackets, offset=3).astype('uint8')
        self._packetTemplate = self.generateIdleTemplate(rowHeight=self._rowHeight, numberOfRows=self._numberOfRows, columnHeight=self._columHeight, forPackets=1, offset=0).astype('uint8')
        self._syncTemplate = self.generateSyncTemplate(rowHeight=self._rowHeight, columnHeight=self._columHeight).astype('uint8')
        
        self._methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
               'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']
        self._method = cv2.TM_CCOEFF_NORMED

    def generateIdleTemplate(self, rowHeight, numberOfRows, columnHeight, forPackets, offset):
        
        template_g = np.repeat(np.array([[[0, 255, 0]]]), rowHeight, axis=0)
        template_r = np.repeat(np.array([[[0, 0, 255]]]), rowHeight, axis=0)
        template_b = np.repeat(np.array([[[255, 0, 0]]]), rowHeight, axis=0)
        template_n = np.repeat(np.array([[[0, 0, 0]]]), rowHeight, axis=0)
        templates = np.array([template_g,template_r,template_b,template_n])
        template_concat = templates[0]
        
        for i in range(1, forPackets*numberOfRows+offset):
            j = i % 4
            template_concat = np.concatenate((template_concat,templates[j]),axis=0)

        template = np.repeat(template_concat, columnHeight, axis=1)
        
        return template

    def generateSyncTemplate(self, rowHeight, columnHeight):
        
        template_g = np.repeat(np.array([[[0, 255, 0]]]), rowHeight, axis=0)
        template_n = np.repeat(np.array([[[0, 0, 0]]]), rowHeight, axis=0)
        # Template
        # --_-_
        template = np.concatenate((template_g, template_g, template_n, template_g, template_n), axis=0)
        template = np.repeat(template, columnHeight, axis=1)
        return template

    #def isSourceDetected(self):
    #return self._isRoiDetected

    def getRoi(self):
        return self._roi

    def getSRoi(self):
        return self._sroi
    
    def getPacket(self):
        return self._packet

    def findSource(self, frame):
        roiPoints = np.array((0, 0, 0, 0))
        isRoiDetected = False
        # 1) Match source template
        res = cv2.matchTemplate(frame, self._sourceTemplate, self._method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        print('DETECTOR: Source found with: {}'.format(res[max_loc[1],max_loc[0]]))
        top_left = max_loc
        h, w, z = self._sourceTemplate.shape
        bottom_right = (top_left[0] + w, top_left[1] + h)

        # 2) Threshold results
        if (res[max_loc[1], max_loc[0]] > 0.70):
            # A Roi has been detected.
            isRoiDetected = True
            self._roi= np.array((top_left, bottom_right))
            roiPoints = self._roi
            print("DETECTOR: ROI has been detected")
        
        return isRoiDetected, roiPoints
    
    def findSRoi(self, frame):
        sroiPoints = np.array((0, 0, 0, 0))
        issRoiDetected = False
        # 1) Match source template
        resRoi = cv2.matchTemplate(frame, self._packetTemplate, self._method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(resRoi)
        # print('DETECTOR: Packet found with: {}'.format(resRoi[max_loc[1],max_loc[0]]))
        # print("RestRoi: {}".format(resRoi[max_loc[1], max_loc[0]]))
        top_left = max_loc
        h, w, z = self._packetTemplate.shape
        bottom_right = (top_left[0] + w, top_left[1] + h)
        
        if (resRoi[max_loc[1], max_loc[0]] > 0.70):
            # A Roi has been stablished.
            issRoiDetected = True
            self._sroi= np.array((top_left, bottom_right))
            sroiPoints = self._sroi
            # A Roi has been destablished
            #self._isRoiDetected = False
        return issRoiDetected, sroiPoints

    def findPacket(self, frame):
        packetPoints = np.array((0, 0, 0, 0))
        isPacketDetected = False
        # 1) Match sync template
        frame_clone = np.zeros(frame.shape)
        frame_clone[:,:,1] = frame[:,:,1]
        frame_clone = frame_clone.astype(np.uint8)
        resRoi = cv2.matchTemplate(frame_clone, self._syncTemplate, self._method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(resRoi)
        #print("SyncRoi: {}".format(resRoi[max_loc[1], max_loc[0]]))
        top_left = max_loc
        h, w, z = self._packetTemplate.shape
        bottom_right = (top_left[0] + w, top_left[1] + h)
        self._packet = np.array((top_left, bottom_right))

        if (resRoi[max_loc[1], max_loc[0]] > 0.70):
            #print("DETECTOR: Sync has been detected")
            #return True, self._packet;
            packetPoints = self._packet
            isPacketDetected = True
        return isPacketDetected, packetPoints