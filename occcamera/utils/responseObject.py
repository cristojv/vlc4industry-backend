#import the necessary packages

'''
file:           responseObject.py
description:
                This file implements the response class
                for generating the response object to
                be sent over the communication to acknoledge
                the consumer the response data.
'''

class ResponseObject:
    def __init__(self, id, status, type, data):
        
        self._id = id
        self._status = status
        self._type = type
        self._data = data
    
    def getId(self):
        return self._id
    def getStatus(self):
        return self._status
    def getType(self):
        return self._type
    def getData(self):
        return self._data