import time
#Decorator timing profiler
def measureTime (function):
    def timerFunction(*args, **kwargs):
        start = time.time()
        result = function(*args, **kwargs)
        end = time.time()
        print('Function {} took {}'.format(function.__name__,end-start))
        return result
    return timerFunction
