#import the necessary packages
from fractions import Fraction
from threading import Thread, Lock
from picamera import PiCamera
from picamera.array import PiRGBArray
import time
import cv2
import collections

class Camera:

    # Attributes:

    def __init__(self, framerate, resolution=(1280,720), exposure = 100):
        
        #print ('CAMERA: Initializing Camera\'s parameters')
        #Parameter Configuration
        self._camera = PiCamera()
        #print ('CAMERA: Resolution -> {}'.format(resolution))
        #1) Resolution -> Must recomend 1280,720 / 16:9 resolution
        self._resolution = resolution
        self._camera.resolution = resolution
        #print ('CAMERA: Framerate -> {}'.format(framerate))
        #2) Framerate -> Number of bands relays on this number
        self._camera.framerate = 15
        #self._camera.framerate = framerate
        #print ('CAMERA: Shutter speed -> {}'.format(framerate))
        #3) Shutter speed -> Needed lower to scan the image
        self._camera.shutter_speed = exposure
        
        self._camera.awb_mode = 'off'
        self._camera.awb_gains = (Fraction(166,100),Fraction(150,100))
        #self._camera.awb_gains = (Fraction(100,100),Fraction(90,100))
        self._rawCapture = None
        self._stream = None
        
        #print("CAMERA: Warming up!")
        #time.sleep(0.5)
        #print("CAMERA: Finished warming")
        
        # Initialize indicators or flags
        self._isRunning = False
        
        # Thread
        self._thread = None
        
        # Buffer
        self._input_deque = collections.deque(maxlen=50)
        self._count = 0
        #self._frame=None
        
    def start(self):
        #print("Start")
        self._isRunning = True
        if self._thread == None:
            self._rawCapture = PiRGBArray(self._camera, size=self._resolution)
            self._stream = self._camera.capture_continuous(self._rawCapture, format='bgr', use_video_port = True)
            time.sleep(0.5)
            self._camera.start_preview()
            print("Create stream")
            self._thread = Thread(target = self.update, args=())
            self._thread.daemon = True
            self._thread.start()
        print('CAMERA: Started')
        #print(self._isRunning)
        return self

    def update(self):
        while(self._isRunning):
            for frame in self._stream:
                self._input_deque.append(frame.array)
                self._count += 1
                self._rawCapture.truncate(0)
                if(self._count>=30):
                    print("ENTRANDO> {}".format(self._count))
                if not self._isRunning:
                    print("IM Here")
                    self._stream.close()
                    self._rawCapture.close()
                    self._stream = None
                    self._rawCapture = None
                    #self._camera.close()
                    print('CAMERA: Stop')
                    return
        return

    def isRunning(self):
        return self._isRunning

    def isThereANewFrame(self):
        return not self._isProcessed

    def grabNextFrame(self):
        global count
        frame = None
        
        if(self._input_deque):
            frame = self._input_deque.popleft().copy()
            self._count = self._count - 1
        else:
            #print("There is no elements within the queue")
            frame = None
            self._count = 0
        return frame

    def stop(self):
        self._isRunning = False
        if self._thread.is_alive():
            print("Join Thread")
            self._thread.join()
            self._thread = None
            self._camera.stop_preview()
            print("Thread stop")
        else:
            print("Thread has not been stoped")
        		
    def flush(self):
        self._count = 0
        self._input_deque.clear()