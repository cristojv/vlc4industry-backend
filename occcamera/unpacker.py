#import the necessary packages
import ctypes
c_uint8 = ctypes.c_uint8

class PackedBits(ctypes.LittleEndianStructure):
    _fields_ = [
        ("length", c_uint8, 4),
        ("response",c_uint8, 2),
        ("id",c_uint8,2)
        ]
class StartPacket(ctypes.Union):
    _anonymous_ = ("bit",)
    _fields_ = [
        ("bit", PackedBits),
        ("asByte", c_uint8)
        ]

def unpack(packet):
    startPacket = StartPacket()
    startPacket.asByte = packet
    return startPacket