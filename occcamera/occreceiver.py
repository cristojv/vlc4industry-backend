#import the necessary packages
from .camera_threaded import Camera
from .detector import Detector
from .calibrator import Calibrator
from .demodulator import Demodulator
from .unpacker import unpack
from events.cameraevents import CameraEvent, CameraEvents
from events.coordinatorevents import CoordinatorEvent, CoordinatorCameraEvent, CoordinatorEvents
import cv2
import matplotlib.pyplot as plt
import numpy as np

from enum import Enum
from multiprocessing import Process, SimpleQueue
import time

class CameraStates(Enum):
	INIT = 1
	SOURCING = 2
	CALIBRATING = 3
	RECEIVING_START = 4
	RECEIVING_DATA = 5
	
class OCCReceiver(Process):
	def __init__(self, inputQueue, outputQueue):
		super(OCCReceiver, self).__init__()
		self._inputQueue = inputQueue;
		self._outputQueue = outputQueue;
	
	def run(self):
                #TODO Remove File
                #f = open("a.txt", "w")
                somethingIsFound = 0
                '''*****************************************
		Initializes variables
		*****************************************'''
                c_resolution = (640,480) #pixels
                c_frameRate = 30 #frames per second
                c_switchingFrequency = 1800 #Hz
                c_exposureTime = 100 #configuration parameter
                c_numberOfPackets = 2
                c_numberOfRows = 5 #packet lenght in number of bands
                c_rowHeight = -(-c_resolution[0]*9*c_frameRate//(16*c_switchingFrequency))#number of row pixels per band
                c_columnHeight = 7#number of column pixels considered for correlation
                '''*****************************************
		Global state variables
		*****************************************'''
                g_frame = None #Saves the frame
                g_roi = None #Saves the roi
                g_packet = None #Saves the packet for demodulation
                g_roiPoints = None #Points of the roi
                g_sroiPoints = None #Points of the small roi // idle packet
                g_state = CameraStates.INIT #State of the camera
                g_isRunning = True #Running Flag
                g_frameProcessed = False #Frame is processed Flag
                g_somethingDetected = False #Something is detected Flagg
                g_response = None #Response obtained after demodulation
                g_packetByte = None #Packet after demodulation in Bytes

                g_roiFound = False #Roi has been found Flag
                g_MAXSourcingIteration = 5 #Const for max of iterations before considering roi is not found.
                g_sourcingIteration = 0 #Number of iterations for sourcing the roi

                g_sroiFound = False #Small roi has been found Flag
                g_MAXsroiIteration = 30 #Const for max of iterations before considering the roi has been lost.
                g_sroiIteration = 0 #Number of iterations for keeping the roi
               
                g_dataIterationMAX = 7 #Const for max of iterations before considering a packet of data has been lost.
                g_dataIteration = 0 #Number of iterations used for outgoing the RECEIVING DATA
                
                g_MAXCalibratingIteration = 100 #Const for max of iterations before calibrating the camera 
                g_calibratingIteration = 0 #Number of iterations for calibrating the camera

                camera = Camera(c_frameRate,c_resolution, exposure=c_exposureTime)

                detector =   Detector(rowHeight=c_rowHeight,\
                                      numberOfRows=c_numberOfRows,\
                                      columnHeight=c_columnHeight,
                                      numberOfPackets=c_numberOfPackets)

                calibrator = Calibrator(rowHeight=c_rowHeight,\
                            numberOfRows=c_numberOfRows,\
                            columnHeight=c_columnHeight,\
                            numberOfPackets=c_numberOfPackets,\
                            numberOfFramesForCalibration=100)

                demodulator = Demodulator()

                isRunning = True

                while(isRunning):
                    time.sleep(0.01)
                    if(g_state == CameraStates.INIT):
                        '''*****************************************
                        Init state
                        *****************************************'''
                        if(self._inputQueue.empty()):
                            time.sleep(0.01)
                        else:
                            cameraEvent = self._inputQueue.get()
                            if isinstance(cameraEvent, CameraEvent):
                                if cameraEvent.getId() == CameraEvents.CAMERA_LAUNCH:
                                    # Init all the global variables
                                    g_frame = None
                                    g_roi = None
                                    g_packet = None
                                    g_roiPoints = None
                                    g_sroiPoints = None
                                    g_frameProcessed = False
                                    g_somethingDetected = False
                                    g_response = None
                                    g_packetByte = None
                                    g_roiFound = False
                                    g_sourcingIteration = 0
                                    g_sroiFound = False
                                    g_sroiIteration = 0
                                    g_dataIteration = 0
                                    g_calibratingIteration = 0
                                    g_state = CameraStates.SOURCING
                                    camera.start()
                            elif cameraEvent.getId() == CameraEvents.CAMERA_STOP:
                                    camera.stop()
                                    camera.flush()
                                    g_state = CameraStates.INIT
                    elif(g_state == CameraStates.SOURCING):
                            '''*****************************************
                            Sourcing state
                            *****************************************'''
                            if not self._inputQueue.empty():
                                cameraEvent = self._inputQueue.get()
                                if isinstance(cameraEvent, CameraEvent):
                                    if cameraEvent.getId() == CameraEvents.CAMERA_STOP:
                                        camera.stop()
                                        camera.flush()
                                        g_state = CameraStates.INIT
                            if(g_frameProcessed):
                                    g_frameProcessed = False;
                                    
                                    if(g_roiFound):
                                            g_roiFound = False
                                            g_sourcingIteration = 0
                                            g_state = CameraStates.CALIBRATING
                                            camera.flush()
                                    else:

                                        if(g_sourcingIteration < g_MAXSourcingIteration):
                                            g_sourcingIteration += 1
                                            g_state = CameraStates.SOURCING
                                            camera.flush()
                                        else:
                                            g_sourcingIteration=0;
                                            g_state = CameraStates.INIT;
                                            camera.stop()
                                            camera.flush()
                                            self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROINOTFOUND))						
                    
                    elif(g_state == CameraStates.CALIBRATING):
                            '''*****************************************
                            Calibrating state
                            *****************************************'''
                            if not self._inputQueue.empty():
                                cameraEvent = self._inputQueue.get()
                                if isinstance(cameraEvent, CameraEvent):
                                    if cameraEvent.getId() == CameraEvents.CAMERA_STOP:
                                        camera.stop()
                                        camera.flush()
                                        g_state = CameraStates.INIT
                            if(g_frameProcessed):
                                g_frameProcessed = False
                                
                                if(g_sroiFound):
                                    g_sroiFound = False
                                    if(g_calibratingIteration<g_MAXCalibratingIteration):
                                        g_calibratingIteration += 1
                                        g_sroiIteration = 0
                                        g_state = CameraStates.CALIBRATING;
                                        calibrator.calibrateChannels(g_roi,g_sroiPoints)
                                    else:
                                        g_calibratingIteration = 0
                                        g_sroiIteration = 0
                                        g_state = CameraStates.RECEIVING_START;
                                        self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROIFOUND))
                                        calibrator.polinomialFittingCalibration()
                                else:
                                    if(g_sroiIteration < g_MAXsroiIteration):
                                        g_calibratingIteration = 0
                                        g_sroiFound = False
                                        g_sroiIteration += 1
                                        g_state = CameraStates.CALIBRATING;
                                    else:
                                        g_somethingDetected = None;
                                        g_roiFound = False;
                                        g_sourcingIteration = 0;
                                        g_sroiFound = False;
                                        g_sroiIteration = 0;
                                        g_calibratingIteration = 0;
                                        g_state = CameraStates.INIT;
                                        camera.stop()
                                        camera.flush()
                                        self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROINOTFOUND))
                    
                    elif(g_state == CameraStates.RECEIVING_START):
                            '''*****************************************
                            Receiving START state
                            *****************************************'''
                            if not self._inputQueue.empty():
                                cameraEvent = self._inputQueue.get()
                                if isinstance(cameraEvent, CameraEvent):
                                    if cameraEvent.getId() == CameraEvents.CAMERA_STOP:
                                        camera.stop()
                                        camera.flush()
                                        g_state = CameraStates.INIT
                            if(g_frameProcessed):
                                g_frameProcessed = False
                                if(g_somethingDetected):
                                    g_somethingDetected = False
                                    g_sroiIteration = 0
                                    if not (g_packetByte==None):
                                        startPacket = unpack(g_packetByte)
                                        
                                        if(startPacket.bit.response == 2): #ACK
                                            self._outputQueue.put(CoordinatorCameraEvent(CoordinatorEvents.CAMERA_RESPONSEOK,\
                                                                  startPacket.bit.id,\
                                                                  startPacket.bit.response,\
                                                                  None))
                                            g_state = CameraStates.RECEIVING_START
                                        
                                        elif(startPacket.bit.response == 1): #ERR
                                            self._outputQueue.put(CoordinatorCameraEvent(CoordinatorEvents.CAMERA_RESPONSEOK,\
                                                                  startPacket.bit.id,\
                                                                  startPacket.bit.response,\
                                                                  None))
                                            g_state = CameraStates.RECEIVING_START
                                        
                                        elif(startPacket.bit.response == 3): #DATA
                                            g_dataId = startPacket.bit.id
                                            g_dataList = []
                                            g_dataLength = startPacket.bit.length
                                            g_dataIteration = 0
                                            g_state = CameraStates.RECEIVING_DATA
                                else:
                                    if(g_sroiFound):
                                        g_sroiFound = False
                                    else:
                                        if(g_sroiIteration < g_MAXsroiIteration):
                                            g_sroiIteration += 1
                                            g_state = CameraStates.RECEIVING_START;
                                        else:
                                            # #TODO Lost ROI
                                            # g_somethingDetected = None;
                                            # g_roiFound = False;
                                            # g_sourcingIteration = 0;
                                            # g_sroiFound = False;
                                            # g_sroiIteration = 0;
                                            # g_calibratingIteration = 0;
                                            # g_state = CameraStates.INIT;
                                            # camera.stop()
                                            # camera.flush()
                                            #Inform the user
                                            self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROILOST))


                    
                    elif(g_state == CameraStates.RECEIVING_DATA):
                        '''*****************************************
                        Receiving DATA state
                        *****************************************'''
                        if not self._inputQueue.empty():
                                cameraEvent = self._inputQueue.get()
                                if isinstance(cameraEvent, CameraEvent):
                                    if cameraEvent.getId() == CameraEvents.CAMERA_STOP:
                                        camera.stop()
                                        camera.flush()
                                        g_state = CameraStates.INIT
                        if(g_frameProcessed):
                            g_frameProcessed = False
                            
                            if(g_somethingDetected):
                                g_somethingDetected = False
                                
                                g_dataIteration = 0
                                if not(g_packetByte==None):
                                    print("DATA {}:> {}".format(g_dataLength, g_packetByte))
                                    
                                    g_dataList.append(g_packetByte)
                                    g_dataLength-=1;
                                    
                                    if(g_dataLength<=0):
                                        self._outputQueue.put(CoordinatorCameraEvent(CoordinatorEvents.CAMERA_RESPONSEOK,\
                                                              g_dataId,\
                                                              3,\
                                                              g_dataList))
                                        g_state = CameraStates.RECEIVING_START
                                    else:
                                        g_state = CameraStates.RECEIVING_DATA 
                            else:
                                g_dataIteration += 1;
                                if(g_dataIteration>= g_dataIterationMAX):
                                    self._outputQueue.put(CoordinatorCameraEvent(CoordinatorEvents.CAMERA_RESPONSEERROR,\
                                                            g_dataId,\
                                                            3,\
                                                            None))
                                    g_state = CameraStates.RECEIVING_START
                                else:
                                    g_state = CameraStates.RECEIVING_DATA 
                                # g_sroiIteration = 0

                    time.sleep(0.01);
                    if(camera.isRunning()):
                            '''1) Grab a new frame
                            2) Detection algorithm
                            2.1)SOURCE: If there is NO source detected. Find a new Source and lock it.
                            If there is source locked. Proceed to correlate packets.
                            2.2)DATA:   Using the ROI computed previously, determine if
                            there is data being sent. If there is data proceed to
                            demodulate the signal.
                            2.3)LOCK:   If there is NO data being sent, check if the source is
                            still locked.
                            '''
                            g_frame =camera.grabNextFrame()
                            if g_frame is not None:
                                g_frameProcessed = True
                                if (g_state == CameraStates.INIT):
                                    pass
                                
                                elif(g_state == CameraStates.SOURCING):
                                    g_roiFound, g_roiPoints = detector.findSource(g_frame)
                                    print(g_roiPoints)
                                
                                elif(g_state == CameraStates.CALIBRATING):
                                    g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
                                    g_roi = cv2.GaussianBlur(g_roi,(5,5),0)
                                    g_sroiFound, g_sroiPoints = detector.findSRoi(g_roi)
                                    print(g_sroiFound)
                                elif(g_state == CameraStates.RECEIVING_START or g_state == CameraStates.RECEIVING_DATA):
                                    g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
                                    g_roi = cv2.GaussianBlur(g_roi,(5,5),0)
                                    packetFound, packetPoints = detector.findPacket(g_roi)
                                    if packetFound:
                                        packet = g_roi[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]]
                                        inv_color, inv_space = calibrator.getChannelsCalibration()
                                        inv_color_roi = inv_color[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                                        inv_space_roi = inv_space[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                                        thresholdLevel = calibrator.getChannelThreshold()
                                        thresholdLevel = thresholdLevel[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                                        packetMean = np.mean(packet, axis=1)
                                        status, enhancedSignal = demodulator.enhanceSignal(packetMean, inv_color_roi, inv_space_roi)
                                        status,id,demodulatedBits = demodulator.demodulateSignal(enhancedSignal.astype(np.uint8),thresholdLevel)
                                        if(status==1):
                                            statusDemodulation,charv = demodulator.convertByteToChar(demodulatedBits)
                                            g_packetByte = charv
                                            print(g_packetByte)
                                            g_somethingDetected = True
                                        elif(status == 3):
                                            statusDemodulation,charv = demodulator.convertByteToChar(demodulatedBits)
                                    else:
                                        g_sroiFound, g_sroiPoints = detector.findSRoi(g_roi)
                isRunning = False
'''        
if __name__ == "__main__":
	occReceiver = OCCReceiver()
	occReceiver.start()
	while not occReceiver.camera.isRunning():
		#print(occReceiver.camera.isRunning())
		pass
	occReceiver.stop()
	occReceiver.join()
	print('Has been joined')
'''