#import the necessary packages

import numpy as np
import matplotlib.pyplot as plt

class Calibrator:
    
    def __init__(self, numberOfRows,rowHeight,columnHeight,numberOfPackets,numberOfFramesForCalibration):
        self._numberOfRows = numberOfRows
        self._rowHeight = rowHeight
        self._halfRowHeight = rowHeight//2
        self._columnHeight = columnHeight
        self._numberOfPackets = numberOfPackets
        self.resetCalibration()

        self._numberOfFramesForCalibration = numberOfFramesForCalibration
        self._isFirstCalibrationDone = 0
        self._framesCount = 0
        self._inv_color_matrix = 0
        self._inv_spatial_matrix = 0
        self._thresholdLevel = 0

        #self._figure = plt.figure();
        #self.graph = self._figure.add_subplot(311);
        #self.graph1 = self._figure.add_subplot(312);
        #self.graph2 = self._figure.add_subplot(313);
        #plt.ion();
    def polinomialFitting(self,calibration):
        x = np.arange(len(calibration))
        idx1 = np.isfinite(calibration[:,1]);
        idx0 = np.isfinite(calibration[:,0]);
        idx2 = np.isfinite(calibration[:,2]);
        #weights_g = calibration[:,1]
        # = calibration[:,0]
        #weights_r = calibration[:,2]
        #weights_g[weights_g < 2] = 0.01
        #weights_b[weights_b < 2] = 0.01
        #weights_r[weights_r < 2] = 0.01
        #weights_g[weights_g >= 2] = 0.99
        #weights_b[weights_b >= 2] = 0.99
        #weights_r[weights_r >= 2] = 0.99
        #fitting_g = np.polyfit(x, calibration[:,1], 1, w=weights_g)
        #fitting_b = np.polyfit(x, calibration[:,0], 1, w=weights_b)
        #fitting_r = np.polyfit(x, calibration[:,2], 1, w=weights_r)
        
        fitting_g = np.polyfit(x[idx1], calibration[idx1,1], 1)
        fitting_b = np.polyfit(x[idx0], calibration[idx0,0], 1)
        fitting_r = np.polyfit(x[idx2], calibration[idx2,2], 1)
        fitter_g = np.poly1d(fitting_g)
        fitter_b = np.poly1d(fitting_b)
        fitter_r = np.poly1d(fitting_r)
        predictions_g = fitter_g(x)
        predictions_b = fitter_b(x)
        predictions_r = fitter_r(x)
        return np.concatenate((predictions_b.reshape(calibration.shape[0],1),predictions_g.reshape(calibration.shape[0],1),predictions_r.reshape(calibration.shape[0],1)),axis=1)

    def calibrateChannels(self,frame,roi_points):
        
        topColumn = roi_points[0][0]
        topRow = roi_points[0][1]
        halfColumnHeight = self._columnHeight//2 

        startingRow = self._halfRowHeight#-self._halfRowHeight//3
        endingRow = self._halfRowHeight+self._halfRowHeight//3
        startingColumn = topColumn + halfColumnHeight - 1
        endColumn = topColumn+halfColumnHeight
        
        rows = endingRow - startingRow
        # Save the new information
        self._greenCalibration[topRow+startingRow:topRow+endingRow]=(\
            frame[topRow+startingRow:topRow+endingRow,\
                  startingColumn:endColumn])\
                  .reshape(rows,3)
        
        
        self._redCalibration[topRow+self._rowHeight+startingRow:topRow+self._rowHeight+endingRow]\
            =(frame[topRow+self._rowHeight+startingRow:topRow+self._rowHeight+endingRow,\
                    startingColumn:endColumn]).reshape(rows,3)
        
        self._blueCalibration[topRow+2*self._rowHeight+startingRow:topRow+2*self._rowHeight+endingRow]\
            =(frame[topRow+2*self._rowHeight+startingRow:topRow+2*self._rowHeight+endingRow,\
                    startingColumn:endColumn]).reshape(rows,3)
        #figure = plt.figure();
        #h = frame[roi_points[0][1]:roi_points[1][1],roi_points[0][0]:roi_points[1][0]]
        #graph = figure.add_subplot(311)
        #graph.plot(h[:,0:1, 0], 'b');
        #graph.plot(h[:,0:1, 1], 'g');
        #graph.plot(h[:,0:1, 2], 'r');
        #plt.show()
    def polinomialFittingCalibration(self):
        #After a number of repetition of frames calculate the polinomial fitting of the 9 iterations.
        #if self._framesCount >= self._numberOfFramesForCalibration - 1:
            #print(self._greenCalibration)
            poly_g = self.polinomialFitting(self._greenCalibration);
            poly_b = self.polinomialFitting(self._blueCalibration);
            poly_r = self.polinomialFitting(self._redCalibration);
            
            #print(poly_g[:,1].shape)
            self._thresholdLevel = np.concatenate(((poly_b[:,0]//2).reshape(poly_g.shape[0],1),(poly_g[:,1]//2).reshape(poly_g.shape[0],1),(poly_r[:,2]//2).reshape(poly_g.shape[0],1)),axis=1)
            #print(self._thresholdLevel.shape)
            #print(self._thresholdLevel[:,0].shape)
            
            #figure = plt.figure();
            #graph = figure.add_subplot(311)
            #graph1 = figure.add_subplot(312)
            #graph2 = figure.add_subplot(313)
            #graph.plot(self._thresholdLevel[:, 0], 'y');
            #graph.plot(self._blueCalibration[:, 0], 'b');
            
            #graph1.plot(self._thresholdLevel[:,1],'y');
            #graph1.plot(self._greenCalibration[:,1],'g');
            
            #graph2.plot(self._thresholdLevel[:,2],'y');
            #graph2.plot(self._redCalibration[:,2],'r');
            
            #plt.show()
            
            self._inv_color_matrix,self._inv_spatial_matrix = self.calibration(poly_b,poly_g,poly_r)
            #if(self._isFirstCalibrationDone==0):
            print("CALIBRATOR: First Calibration has been done")
            #    self._isFirstCalibrationDone = 1
            '''
            figure = plt.figure();
            graph = figure.add_subplot(311)
            graph1 = figure.add_subplot(312)
            graph2 = figure.add_subplot(313)
            graph.plot(self._greenCalibration[:, 0], 'b');
            graph.plot(self._greenCalibration[:, 1], 'g');
            graph.plot(self._greenCalibration[:, 2], 'r');
            graph.plot(poly_g[:,0],'b');
            graph.plot(poly_g[:,1],'g');
            graph.plot(poly_g[:,2],'r');
            graph1.plot(self._blueCalibration[:,0],'b');
            graph1.plot(self._blueCalibration[:,1],'g');
            graph1.plot(self._blueCalibration[:,2],'r');
            graph1.plot(poly_b[:,0],'b');
            graph1.plot(poly_b[:,1],'g');
            graph1.plot(poly_b[:,2],'r');
            graph2.plot(self._redCalibration[:,0],'b');
            graph2.plot(self._redCalibration[:,1],'g');
            graph2.plot(self._redCalibration[:,2],'r');
            graph2.plot(poly_r[:,0],'b');
            graph2.plot(poly_r[:,1],'g');
            graph2.plot(poly_r[:,2],'r');
            plt.show()
            '''
            #plt.pause(0.00001);
        # if __debug__:
        #
        #     # TODO: Take this outside
        #     if self._framesCount == self._numberOfFramesForCalibration-1:
        #         model = Pipeline(
        #             [('poly', PolynomialFeatures(degree=2)), ('linear', LinearRegression(fit_intercept=True))])
        #
        #         x = np.arange(len(self._greenCalibration))
        #         weights = self._greenCalibration
        #         # weights[weights > 0] = 1
        #
        #         fitting_g = np.polyfit(x,self._greenCalibration[:,1],2,w=weights[:,1])
        #         fitting_b = np.polyfit(x,self._greenCalibration[:,0],2,w=weights[:,0])
        #         fitting_r = np.polyfit(x,self._greenCalibration[:,2],2,w=weights[:,2])
        #         fitter_g = np.poly1d(fitting_g)
        #         fitter_b = np.poly1d(fitting_b)
        #         fitter_r = np.poly1d(fitting_r)
        #         predictions_g = fitter_g(x)
        #         predictions_r = fitter_b(x)
        #         predictions_b = fitter_r(x)
        #         # model_g = model.fit(x[:, np.newaxis],self._greenCalibration[:,1],sample_weight=weights)
        #         # model_r = model.fit(x[:, np.newaxis],self._greenCalibration[:,0],sample_weight=weights)
        #         # model_b = model.fit(x[:, np.newaxis],self._greenCalibration[:,2],sample_weight=weights)
        #
        #         # predictions_g = model_g.predict(x[:, np.newaxis])
        #         # predictions_r = model_r.predict(x[:, np.newaxis])
        #         # predictions_b = model_b.predict(x[:, np.newaxis])
        #         # TODO: Take this outside
        #
        #         self.graph.plot(predictions_g,'c');
        #         self.graph.plot(predictions_r,'o');
        #         self.graph.plot(predictions_b,'y');
        #
        #     plt.pause(0.00000000000001);

        #self._framesCount = (self._framesCount + 1)%self._numberOfFramesForCalibration
        #print('salido')
    
    def concatenateMatrix(self, blue, green, red):
        number_samples = blue.shape[0];
        inverse_matrix = []
        stack_matrix = []
        current_matrix = []
        for i in range(0, number_samples):
            current_matrix = np.concatenate(
                (blue[i, :].reshape(1, 3), green[i, :].reshape(1, 3), red[i, :].reshape(1, 3)), axis=0).reshape(1, 3, 3)
            try:
                inverse = np.linalg.inv(current_matrix)
            except np.linalg.LinAlgError:
                print("Not invertible");
                pass
            else:
                # continue with what you were doing
                if (i == 0):
                    inverse_matrix = inverse
                    stack_matrix = current_matrix
                else:
                    inverse_matrix = np.concatenate((inverse_matrix, inverse))
                    stack_matrix = np.concatenate((stack_matrix, current_matrix))
        return stack_matrix, inverse_matrix
    
    def calibration(self, blue_contents, green_contents, red_contents, debug=0):
        # Return values
        self._inv_color_matrix = 0
        self._inv_spatial_matrix = 0

        number_samples = blue_contents.shape[0];
        # Calculate channel interference matrix
        blue_division = np.divide(blue_contents, blue_contents[:, 0].reshape(number_samples, 1));
        green_division = np.divide(green_contents, green_contents[:, 1].reshape(number_samples, 1));
        red_division = np.divide(red_contents, red_contents[:, 2].reshape(number_samples, 1));

        stack_matrix, inv_color_matrix = self.concatenateMatrix(blue_division, green_division, red_division)

        # Calculate spatial response
        blue_spatial = 1 / (np.array(blue_contents[:, 0], dtype='float') / np.max(blue_contents[:, 0])).reshape(
            number_samples, 1)
        green_spatial = 1 / (np.array(green_contents[:, 1], dtype='float') / np.max(green_contents[:, 1])).reshape(
            number_samples, 1)
        red_spatial = 1 / (np.array(red_contents[:, 2], dtype='float') / np.max(red_contents[:, 2])).reshape(
            number_samples, 1)
        inv_spatial_matrix = np.concatenate((blue_spatial, green_spatial, red_spatial), axis=1);
        return inv_color_matrix, inv_spatial_matrix

    def resetCalibration(self):
        #self._blueCalibration = np.ones(((self._numberOfRows)*self._rowHeight*self._numberOfPackets+3*self._rowHeight,3))
        #self._greenCalibration = np.ones(((self._numberOfRows)*self._rowHeight*self._numberOfPackets+3*self._rowHeight,3))
        #self._redCalibration = np.ones(((self._numberOfRows)*self._rowHeight*self._numberOfPackets+3*self._rowHeight,3))
        self._blueCalibration = np.empty(((self._numberOfRows)*self._rowHeight*self._numberOfPackets+3*self._rowHeight,3))
        self._greenCalibration = np.empty(((self._numberOfRows)*self._rowHeight*self._numberOfPackets+3*self._rowHeight,3))
        self._redCalibration = np.empty(((self._numberOfRows)*self._rowHeight*self._numberOfPackets+3*self._rowHeight,3))
        self._blueCalibration[:] = np.nan
        self._greenCalibration[:] = np.nan
        self._redCalibration[:] = np.nan

        #print(self._blueCalibration.shape)
        self._framesCount = 0;

    def getChannelsCalibration(self):
        return self._inv_color_matrix, self._inv_spatial_matrix
    
    def getChannelThreshold(self):
        return self._thresholdLevel
    
    def isFirstCalibrationDone(self):
        return self._isFirstCalibrationDone