#import the necessary packages
from camera_threaded import Camera
from detector import Detector
from calibrator import Calibrator
from demodulator import Demodulator
from unpacker import unpack
from events.cameraevents import CameraEvent, CameraEvents
from events.coordinatorevents import CoordinatorEvent, CoordinatorCameraEvent, CoordinatorEvents
import cv2
import matplotlib.pyplot as plt
import numpy as np

from enum import Enum
from multiprocessing import Process, SimpleQueue
import time

class CameraStates(Enum):
	INIT = 1
	SOURCING = 2
	CALIBRATING = 3
	RECEIVING_START = 4
	RECEIVING_DATA = 5
	
class OCCReceiver(Process):
	def __init__(self, inputQueue, outputQueue):
		super(OCCReceiver, self).__init__()
		self._inputQueue = inputQueue;
		self._outputQueue = outputQueue;
	
	def run(self):
                #TODO Remove File
                #f = open("a.txt", "w")
                somethingIsFound = 0
                '''*****************************************
		Initializes variables
		*****************************************'''
                c_resolution = (640,480) #pixels
                c_frameRate = 30 #frames per second
                c_switchingFrequency = 1800 #Hz
                c_exposureTime = 100 #configuration parameter
                c_numberOfPackets = 2
                c_numberOfRows = 5 #packet lenght in number of bands
                c_rowHeight = -(-c_resolution[0]*9*c_frameRate//(16*c_switchingFrequency))#number of row pixels per band
                c_columnHeight = 7#number of column pixels considered for correlation
                '''*****************************************
		Global state variables
		*****************************************'''
                g_frame = None
                g_roi = None
                g_packet = None
                g_roiPoints = None
                g_sroiPoints = None
                g_state = CameraStates.INIT
                g_isRunning = True
                g_frameProcessed = False
                g_somethingDetected = False
                g_response = None
                g_packetByte = None
                g_roiFound = False
                g_MAXSourcingIteration = 5
                g_sourcingIteration = 0
                g_sroiFound = False
                g_MAXsroiIteration = 30
                g_sroiIteration = 0
                g_MAXCalibratingIteration = 100
                g_calibratingIteration = 0
                camera = Camera(c_frameRate,c_resolution, exposure=c_exposureTime)
                detector =   Detector(rowHeight=c_rowHeight,\
                                      numberOfRows=c_numberOfRows,\
                                      columnHeight=c_columnHeight,
                                      numberOfPackets=c_numberOfPackets)
                calibrator = Calibrator(rowHeight=c_rowHeight,\
                            numberOfRows=c_numberOfRows,\
                            columnHeight=c_columnHeight,\
                            numberOfPackets=c_numberOfPackets,\
                            numberOfFramesForCalibration=100)
                demodulator = Demodulator()
                isRunning = True
                while(isRunning):
                    time.sleep(0.01);
                    #print(g_state)
                    if(g_state == CameraStates.INIT):
                        '''*****************************************
                        Init state
                        *****************************************'''
                        if(self._inputQueue.empty()):
                            pass
                        else:
                            cameraEvent = self._inputQueue.get()
                            if isinstance(cameraEvent, CameraEvent):
                                if cameraEvent.getId() == CameraEvents.CAMERA_LAUNCH:
                                    g_frameProcessed = False;
                                    g_somethingDetected = None;
                                    g_roiFound = False;
                                    g_sourcingIteration = 0;
                                    g_sroiFound = False;
                                    g_sroiIteration = 0;
                                    g_calibratingIteration = 0;
                                    g_state = CameraStates.SOURCING;
                                    camera.start()
                                    pass
                    elif(g_state == CameraStates.SOURCING):
                            '''*****************************************
                            Sourcing state
                            *****************************************'''
                            if(g_frameProcessed):
                                    
                                    if(g_roiFound):
                                            g_sourcingIteration = 0;
                                            g_frameProcessed = False;
                                            g_state = CameraStates.CALIBRATING;
                                            camera.flush()
                                            #g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
                                            #cv2.imshow('frame',g_frame)
                                            #cv2.imshow('roi',g_roi)
                                            #cv2.waitKey(0)
                                    else:
                                            
                                            if(g_sourcingIteration < g_MAXSourcingIteration):
                                                    g_sourcingIteration += 1
                                                    g_frameProcessed = False
                                                    g_state = CameraStates.SOURCING
                                                    camera.flush()
                                            else:
                                                    g_sourcingIteration=0;
                                                    g_frameProcessed = False;
                                                    g_state = CameraStates.INIT;
                                                    camera.stop()
                                                    camera.flush()
                                                    self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROINOTFOUND))						
                    elif(g_state == CameraStates.CALIBRATING):
                            '''*****************************************
                            Calibrating state
                            *****************************************'''
                            if(g_frameProcessed):
                                    if(g_sroiFound):
                                            if(g_calibratingIteration<g_MAXCalibratingIteration):
                                                    g_calibratingIteration += 1
                                                    g_frameProcessed = False
                                                    g_sroiFound = False
                                                    g_sroiIteration = 0
                                                    g_state = CameraStates.CALIBRATING;
                                                    calibrator.calibrateChannels(g_roi,g_sroiPoints)
                                                    #TODO Insert Data for Calibration
                                            else:
                                                    g_calibratingIteration = 0
                                                    g_frameProcessed = False
                                                    g_sroiFound = False
                                                    g_sroiIteration = 0
                                                    g_state = CameraStates.RECEIVING_START;
                                                    camera._camera.stop_preview() #TODO remove
                                                    self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROIFOUND))
                                                    calibrator.polinomialFittingCalibration()
                                                    #TODO Polynomial Fitting for Calibration
                                    else:
                                            if(g_sroiIteration < g_MAXsroiIteration):
                                                    g_calibratingIteration = 0
                                                    g_frameProcessed = False
                                                    g_sroiFound = False
                                                    g_sroiIteration += 1
                                                    g_state = CameraStates.CALIBRATING;
                                            else:
                                                    g_frameProcessed = False;
                                                    g_somethingDetected = None;
                                                    g_roiFound = False;
                                                    g_sourcingIteration = 0;
                                                    g_sroiFound = False;
                                                    g_sroiIteration = 0;
                                                    g_calibratingIteration = 0;
                                                    g_state = CameraStates.INIT;
                                                    camera.stop()
                                                    camera.flush()
                                                    self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROINOTFOUND))
                    elif(g_state == CameraStates.RECEIVING_START):
                            '''*****************************************
                            Receiving state
                            *****************************************'''
                            #print("Receiving State")
                            if(g_frameProcessed):
                                if(g_somethingDetected):
                                    if not (g_packetByte==None):
                                        startPacket = unpack(g_packetByte)
                                        if(startPacket.bit.response == 1): #ACK
                                            self._outputQueue.put(CoordinatorCameraEvent(CoordinatorEvents.CAMERA_RESPONSEOK,\
                                                                  startPacket.bit.id,\
                                                                  startPacket.bit.response,\
                                                                  None))
                                            g_sroiIteration = 0
                                            g_state = CameraStates.RECEIVING_START
                                            g_somethingDetected = False
                                            
                                            
                                g_frameProcessed = False
                    
                    time.sleep(0.01);
                    if(camera.isRunning()):
                            #print(g_state)
                            '''1) Grab a new frame
                            2) Detection algorithm
                            2.1)SOURCE: If there is NO source detected. Find a new Source and lock it.
                            If there is source locked. Proceed to correlate packets.
                            2.2)DATA:   Using the ROI computed previously, determine if
                            there is data being sent. If there is data proceed to
                            demodulate the signal.
                            2.3)LOCK:   If there is NO data being sent, check if the source is
                            still locked.
                            '''
                            g_frame =camera.grabNextFrame()
                            #print("No hay frame")
                            if g_frame is not None:
                                #print("Cogiendo frame")
                                g_frameProcessed = True
                                if (g_state == CameraStates.INIT):
                                    pass
                                
                                elif(g_state == CameraStates.SOURCING):
                                    g_roiFound, g_roiPoints = detector.findSource(g_frame)
                                    print(g_roiPoints)
                                
                                elif(g_state == CameraStates.CALIBRATING):
                                    g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
                                    g_roi = cv2.GaussianBlur(g_roi,(5,5),0)
                                    g_sroiFound, g_sroiPoints = detector.findSRoi(g_roi)
                                    print(g_sroiFound)
                                elif(g_state == CameraStates.RECEIVING_START):
                                    g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
                                    g_roi = cv2.GaussianBlur(g_roi,(5,5),0)
                                    packetFound, packetPoints = detector.findPacket(g_roi)
                                    if packetFound:
                                        #print("Something has been detected")
                                        packet = g_roi[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]]
                                        inv_color, inv_space = calibrator.getChannelsCalibration()
                                        inv_color_roi = inv_color[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                                        inv_space_roi = inv_space[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                                        thresholdLevel = calibrator.getChannelThreshold()
                                        thresholdLevel = thresholdLevel[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                                        packetMean = np.mean(packet, axis=1)
                                        status, enhancedSignal = demodulator.enhanceSignal(packetMean, inv_color_roi, inv_space_roi)
                                        '''
                                        figure = plt.figure();
                                        graph = figure.add_subplot(311)
                                        graph1 = figure.add_subplot(312)
                                        graph.plot(enhancedSignal[:, 0], 'b');
                                        graph.plot(enhancedSignal[:, 1], 'g');
                                        graph.plot(enhancedSignal[:, 2], 'r');
                                        graph.plot(thresholdLevel[:,0],'b');
                                        graph.plot(thresholdLevel[:,1],'g');
                                        graph.plot(thresholdLevel[:,2],'r');
                                        plt.show()'''
                                        status,id,demodulatedBits = demodulator.demodulateSignal(enhancedSignal.astype(np.uint8),thresholdLevel)
                                        if(status==1):
                                            statusDemodulation,charv = demodulator.convertByteToChar(demodulatedBits)
                                            g_packetByte = charv
                                            g_somethingDetected = True
                                            #print("id: {}, m: {}, b: {}".format(id,demodulatedBits,charv))
                                            #f.write("{}, ".format(str(charv)))
                                            #somethingIsFound+=1
                                        elif(status == 3):
                                            statusDemodulation,charv = demodulator.convertByteToChar(demodulatedBits)
                                            #print("id: {}, m: {}, b: {} (With error)".format(id,demodulatedBits,chr(charv)))
                                            #f.write(" {(E)} ")
                                            #f.write(str(charv))
                                            #f.write(" {(E)} ")
                                            #somethingIsFound+=1
                                        #print("Count: {}".format(somethingIsFound))
                                        #if(somethingIsFound==1000):
                                            #camera.stop()
                                            #isRunning = False
                                            #f.flush()
                                            #f.close()
                                    else:
                                        g_sroiFound, g_sroiPoints = detector.findSRoi(g_roi)
                isRunning = False
        
if __name__ == "__main__":
	occReceiver = OCCReceiver()
	occReceiver.start()
	while not occReceiver.camera.isRunning():
		#print(occReceiver.camera.isRunning())
		pass
	occReceiver.stop()
	occReceiver.join()
	print('Has been joined')
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    