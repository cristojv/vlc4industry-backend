from coordinator.coordinatorsm import CoordinatorSM
from events.coordinatorevents import CoordinatorEvents, CoordinatorEvent
from coordinator.coordinatorstates import InitState, LaunchingState, ReceivingState, WaitingResponseState
import unittest

class CoordinatorTest(unittest.TestCase):
	
	def setUp(self):
		self.csm = CoordinatorSM(None,None,None)
	
	def test_bestCase(self):
		event = CoordinatorEvent(CoordinatorEvents.USER_START)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, LaunchingState.__name__, "Incorrect finish state")
		
		event = CoordinatorEvent(CoordinatorEvents.CAMERA_ROIFOUND)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, ReceivingState.__name__, "Incorrect finish state")
		
		event = CoordinatorEvent(CoordinatorEvents.USER_COMMAND)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, WaitingResponseState.__name__, "Incorrect finish state")
		
		event = CoordinatorEvent(CoordinatorEvents.CAMERA_RESPONSEOK)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, ReceivingState.__name__, "Incorrect finish state")
		
		event = CoordinatorEvent(CoordinatorEvents.USER_STOP)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, InitState.__name__, "Incorrect finish state")
			
	def test_CameraResponseError(self):
		events = [
			CoordinatorEvent(CoordinatorEvents.USER_START),
			CoordinatorEvent(CoordinatorEvents.CAMERA_ROIFOUND),
			CoordinatorEvent(CoordinatorEvents.USER_COMMAND),
		]
		
		for event in events:
			self.csm.onEvent(event)
		
		
		event = CoordinatorEvent(CoordinatorEvents.CAMERA_RESPONSEERROR)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, WaitingResponseState.__name__, "Incorrect finish state")
		self.assertEqual(self.csm.getIteration(), 1, "Incorrect iteration number")
		self.assertEqual(self.csm.getCause(), "CAMERA_RESPONSEERROR", "Incorrect cause")
		
		event = CoordinatorEvent(CoordinatorEvents.CAMERA_RESPONSEERROR)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, WaitingResponseState.__name__, "Incorrect finish state")
		self.assertEqual(self.csm.getIteration(), 2, "Incorrect iteration number")
		self.assertEqual(self.csm.getCause(), "CAMERA_RESPONSEERROR", "Incorrect cause")
		
		event = CoordinatorEvent(CoordinatorEvents.CAMERA_RESPONSEERROR)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, ReceivingState.__name__, "Incorrect finish state")
		self.assertEqual(self.csm.getIteration(), 2, "Incorrect iteration number")
		self.assertEqual(self.csm.getCause(), "CAMERA_RESPONSEERROR", "Incorrect cause")

if __name__ == "__main__":
	unittest.main()