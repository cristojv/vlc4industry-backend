#import the necessary packages

from threading import Thread
from multiprocessing import Process, Pipe
import Queue
'''
****************************************************
Multiprocess configuration
****************************************************
'''
'''
****************************************************
name: Response PIPE
description: This pipe is used to evaluate the 
****************************************************
'''

class Controller(Thread):
    def __init__(self,socket,userQUEUE,responsePIPE):
        super(Controller,self).__init__()
        
        self._socket=socket
        self._userQUEUE = Queue.Queue()
        self._responsePIPE = responsePIPE
        self._isRunning = False
    
    def stop(self):
        self._isRunning = False
    
    def run(self):
        self._isRunning = True
        while(self._isRunning):
            #Wait for user command
            