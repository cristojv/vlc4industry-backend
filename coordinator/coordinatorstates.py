#import the necessary packages
from .coordinatorstate import CoordinatorState
from events.coordinatorevents import CoordinatorEvents, CoordinatorEvent, CoordinatorUserEvent
from events.baseevents import BaseEvent
import struct
'''
STATE: 						Init state
DESCRIPTION: 		Initial state. Waiting for user commands

getfrom-events: 	NOTE: This is the initial state of the state machine
										CAMERA_ROINOTFOUND
jumpto-events: 	USER_START -> LaunchingState
										USER_CONFIG -> ConfiguringState
'''
class InitState(CoordinatorState):
	
	def onEvent (self, event):
		if(isinstance(event, CoordinatorEvent)):
			
			if(event.getId() == CoordinatorEvents.USER_START):
				
				#Launch the camera.
				self._coordinator.cameraLaunch()
				self._coordinator.userMessage("La camara esta siendo lanzada en este momento. Espere mientras se calibra el sistema.")
				
				return LaunchingState.__name__
			
			elif(event.getId()== CoordinatorEvents.USER_CONFIG):
				#do some actions
				return ConfiguringState.__name__
		return self.__class__.__name__
	def run(self):
		pass
'''
STATE: 						Launching state
DESCRIPTION: 		The camera is launched and start to capture and 
										processes frames to calibrate the system.
getfrom-events: 	USER_START
jumpto-events: 	CAMERA_ROIFOUND > ReceivingState
										CAMERA_ROINOTFOUND > InitState
'''

class LaunchingState(CoordinatorState):
		
	def onEvent (self, event):
		if(isinstance(event, CoordinatorEvent)):
			
			if(event.getId() == CoordinatorEvents.CAMERA_ROIFOUND):
				#do some actions
				self._coordinator.userStart("La camara ha sido calibrada correctamente. Ya puede comenzar a enviar sus comandos al robot.", "ok")
				return ReceivingState.__name__
			
			elif(event.getId()== CoordinatorEvents.CAMERA_ROINOTFOUND):
				#do some actions
				self._coordinator.userStart("La camara NO ha sido calibrada correctamente. Por favor, asegurese de que se encuentra encendida y dentro del rango de la cámara.", "noOk")
				return InitState.__name__
			elif(event.getId()== CoordinatorEvents.USER_STOP):
				#do some actions
				self._coordinator.cameraStop()
				self._coordinator.userStop("La camara ha sido apagada, para posterior calibracion.")
				return InitState.__name__
		return self.__class__.__name__
	def run(self):
		pass
		
'''
STATE: 						Receiving state
DESCRIPTION: 		The system is waiting for commands to send to the lamp.

state-variables:		id
										iteration
										cause
										
getfrom-events: 	CAMERA_ROIFOUND
jumpto-events: 	USER_COMMAND > WaitingResponseState
										USER_STOP > InitState
'''
class ReceivingState(CoordinatorState):
	
	def __init__(self,coordinator):
		super(ReceivingState, self).__init__(coordinator)
		
	def onEvent (self, event):
		if(isinstance(event, CoordinatorEvent)):
			
			print('ReceivingState accesing variables: id> {}, iteration> {}, cause> {}'.format(self._coordinator.getId(), self._coordinator.getIteration(), self._coordinator.getCause()))
			
			if(event.getId() == CoordinatorEvents.USER_COMMAND):
				if(isinstance(event,CoordinatorUserEvent)):
					#do some actions
					#TODO set ID
					self._coordinator.setIteration(0)
					self._coordinator.setCause(None)
					
					newId = (self._coordinator.getId()+1)%3
					self._coordinator.setId(newId)
					packetId = self._coordinator.getId()
					self._coordinator.setPacketType(event.getType())

					print(str.encode(str(packetId)+str(event.getCommand())+'\n'))
					self._coordinator.sendCommandToTx(str.encode(str(packetId)+str(event.getCommand())+'\n'))
					self._coordinator.userMessage("Enviado comando y esperando respuesta.")
					self._coordinator.enableTimer(3)
				return WaitingResponseState.__name__
			
			elif(event.getId()== CoordinatorEvents.USER_STOP):
				#do some actions
				self._coordinator.cameraStop()
				self._coordinator.userStop("La camara ha sido apagada, para posterior calibracion.")
				return InitState.__name__
			elif(event.getId()== CoordinatorEvents.CAMERA_ROILOST):
				self._coordinator.cameraStop()
				self._coordinator.userStop("La camara ha sido apagada, para posterior calibracion.")
				return InitState.__name__
		return self.__class__.__name__

'''
STATE: 						WaitingResponse state
DESCRIPTION: 		The system is waiting for a response of a command.
NOTE: 							Take into consideration that this state can jump using
										internal state variables.

state-const:				@maxnumberofiterations
state-variables: 	iteration: counts the number of failed attempts.
										A failed attempt is considered when the timeout
										happens or when the event cameraResponseError
										occurs.
										
										errorCause: Saves the exiting cause in case of error
										(timeout, responseError)

getfrom-events: 	USER_COMMAND
										CONTROLLER_RESPONSEERROR
										
jumpto-events: 	CAMERA_RESPONSEOK & sameId > ReceivingState
										CAMERA_RESPONSEERROR & iteration>=@maxnumberofiterations > ReceivingState
										CAMERA_TIMEOUT & iteration>=@maxnumberofiterations > ReceivingState
										
internal-events: 	CAMERA_RESPONSEERROR & iteration<@maxnumberofiterations > self
										CAMERA_TIMEOUT & iteration<@maxnumberofiterations > self
										CAMERA_RESPONSEOK & ! sameId > self
'''
class WaitingResponseState(CoordinatorState):
		
	def __init__(self,coordinator, maxNumberOfIterations = 1):
		super(WaitingResponseState, self).__init__(coordinator)
		self._maxNumberOfIterations = maxNumberOfIterations-1
		
	def onEvent (self, event):
		print("Event")
		
		if(isinstance(event, CoordinatorEvent)):
			# print('ReceivingState accesing variables: id> {}, iteration> {}, cause> {}'.format(self._coordinator.getId(), self._coordinator.getIteration(), self._coordinator.getCause()))
			# print(event.getId())
			if(event.getId() == CoordinatorEvents.CAMERA_RESPONSEOK):
				#do some actions
				print("response ok")
				#analyze ID
				print("EVENTID> {}".format(event.getResponseId()))
				print("CURRENTID> {}".format(self._coordinator.getId()))
				if(event.getResponseId() == self._coordinator.getId()): #TODO Change ID
					if(event.getResponse()==2): #ACK
						print("ack")
						self._coordinator.userACK("OK: Respuesta recibida.")
					elif(event.getResponse()==1): #ERR
						print("err")
						self._coordinator.userERR("ERROR: El robot no ha podido interpretar el commando")
					elif(event.getResponse()==3): #Data
						dato = event.getData()
						print("data")
						lengthData = len(dato)

						if(lengthData==4 and self._coordinator.getPacketType()=='TEMP'):
							temp = struct.pack('4B', *dato)
							floatTemp = struct.unpack('<f',temp)
							print("Float TEMP> {} ".format(floatTemp))
							self._coordinator.userTemp(floatTemp)
						elif(lengthData==2 and self._coordinator.getPacketType()=='LUM'):
							lum = int.from_bytes(dato, byteorder='big')
							self._coordinator.userLum(lum)
						elif(lengthData==6 and self._coordinator.getPacketType()=='ACC'):
							accxDato = [dato[0], dato[1]]
							accyDato = [dato[2], dato[3]]
							acczDato = [dato[4], dato[5]]
							accx = int.from_bytes(accxDato, byteorder='big')
							accy = int.from_bytes(accyDato, byteorder='big')
							accz = int.from_bytes(acczDato, byteorder='big')
							self._coordinator.userACC(accx,accy,accz)
					return ReceivingState.__name__
				else:
					return WaitingResponseState.__name__
			
			elif(event.getId()== CoordinatorEvents.CAMERA_RESPONSEERROR and self._coordinator.getIteration() >= self._maxNumberOfIterations):
				self._coordinator.disableTimer()
				self._coordinator.userERR("[ERROR]: El sistema no ha podido detectar correctamente el dato.")
				return ReceivingState.__name__
			
			elif(event.getId()== CoordinatorEvents.CONTROLLER_TIMEOUT and self._coordinator.getIteration() >= self._maxNumberOfIterations):
				self._coordinator.disableTimer()
				self._coordinator.userERR("[TIMEOUT]: El sistema no ha podido enviar correctamente el commando. Y se han agotado las iteraciones.")
				return ReceivingState.__name__
				
			elif(event.getId()== CoordinatorEvents.CAMERA_RESPONSEERROR and self._coordinator.getIteration() < self._maxNumberOfIterations):
				
				self._coordinator.setIteration(self._coordinator.getIteration()+1)
				self._coordinator.setCause("CAMERA_RESPONSEERROR")
				self._coordinator.userMessage("[ERROR]: El sistema no ha podido detectar correctamente el dato. Reenviando {}".format(self._coordinator.getIteration()))
				# TODO: ENVIAR ARDUINO
				self._coordinator.enableTimer(3)
				return WaitingResponseState.__name__
				
			elif(event.getId()== CoordinatorEvents.CONTROLLER_TIMEOUT and self._coordinator.getIteration() < self._maxNumberOfIterations):
				
				self._coordinator.setIteration(self._coordinator.getIteration()+1)
				self._coordinator.setCause("CONTROLLER_TIMEOUT")
				self._coordinator.userMessage("[TIMEOUT]: El sistema no ha enviado correctamente el comando. Reenviando{}".format(self._coordinator.getIteration()))
				# TODO: ENVIAR ARDUINO
				self._coordinator.enableTimer(3)
				return WaitingResponseState.__name__
			
			elif(event.getId()== CoordinatorEvents.USER_STOP):
				#do some actions
				self._coordinator.disableTimer()
				self._coordinator.cameraStop()
				self._coordinator.userStop("La camara ha sido apagada, para posterior calibracion.")
				return InitState.__name__
			elif(event.getId()== CoordinatorEvents.CAMERA_ROILOST):
				self._coordinator.cameraStop()
				self._coordinator.userStop("La camara ha sido apagada, para posterior calibracion.")
				return InitState.__name__
		return self.__class__.__name__
	def run(self):
		pass

'''
STATE: 						VerifyingResponse state
DESCRIPTION: 		The system verifies the response obtained from the camera
										to assure there is no errors.

getfrom-events: 	CAMERA_RESPONSEOK
										
jumpto-events: 	CONTROLLER_RESPONSEOK > ReceivingState
										CONTROLLER_RESPONSEERROR > WaitingResponseState

class VerifyingResponseState(ControllerState):
	
	def __init__(self,id):
		super(VerifyingResponseState, self).__init__(self)
		self._id = id
		
	def onEvent (self, event):
		if(isinstance(event, CoordinatorEvent):
			
			if(event.getId()) == SMEvents.CONTROLLER_RESPONSEOK):
				#do some actions
				return ReceivingState.__name__
			
			elif(event.getId()== SMEvents.CONTROLLER_RESPONSEERROR):
				#do some actions
				return WaitingResponseState.__name__
	def run(self):
		pass
'''