#import the necessary packages
from .coordinatorstates import InitState, LaunchingState, ReceivingState, WaitingResponseState
from threading import Thread
import time

from events.cameraevents import CameraEvents, CameraEvent
'''
file: coordinatorsm.py
description:		Implements the class CoordinatorSM that represents the state machine.
'''
class CoordinatorSM():
	def __init__(self, toCameraQueue,toUserSocket,toArduinoSerial,timerFD):
		self._id = 1
		self._iteration = 0
		self._cause = None
		self._packetType = None
		
		self._toCameraQueue = toCameraQueue
		self._toUserSocket = toUserSocket
		self._toArduinoSerial = toArduinoSerial
		self._timerFD = timerFD
		
		self._states = {InitState.__name__ : InitState(self)}
		self._states[LaunchingState.__name__] = LaunchingState(self)
		self._states[ReceivingState.__name__] = ReceivingState(self)
		self._states[WaitingResponseState.__name__] = WaitingResponseState(self)
		self._state = InitState.__name__
	
	def onEvent(self, event):
		print(event.getId())
		self._state = self._states[self._state].onEvent(event)
		print('Changing state> {}'.format(self._state))
	
	def getState(self):
		return self._states[self._state]
	
	def cameraLaunch(self):
		if not self._toCameraQueue == None:
			self._toCameraQueue.put(CameraEvent(CameraEvents.CAMERA_LAUNCH))
			print("Launching Camera event")
	
	def cameraStop(self):
		if not self._toCameraQueue == None:
			self._toCameraQueue.put(CameraEvent(CameraEvents.CAMERA_STOP))
	
	def userMessage(self, message):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('messageClient', {'msg': message}, namespace='/main')
	
	def userStart(self, message, isOk):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('startClient', {'msg': message, 'isOk': isOk}, namespace='/main')
	
	def userStop(self, message):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('stopClient', {'msg': message}, namespace='/main')

	def userACK(self,message):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('ackClient', {'msg': message}, namespace='/main')		
	def userERR(self,message):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('errClient', {'msg': message}, namespace='/main')		
	def userDAT(self,message):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('datClient', {'msg': message}, namespace='/main')
	
	def userTemp(self,temp):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('tempClient', {'temp': temp}, namespace='/main')

	def userLum(self, lum):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('lumClient', {'lum': lum}, namespace='/main')

	def userACC(self, accx, accy, accz):
		if not self._toUserSocket == None:
			self._toUserSocket.emit('accClient', {'accx': accx, 'accy': accy, 'accz': accz}, namespace='/main')

	def sendCommandToTx(self, command):
		if not self._toArduinoSerial== None:
			self._toArduinoSerial.write(command)
			# arduino = self._toArduinoSerial.read(100)
			# self._toUserSocket.emit('messageClient', {'msg': arduino.decode("utf-8")}, namespace='/main')
	
	def enableTimer(self,timeInSeconds):
		self._timerFD.settime(timeInSeconds,0)
	def disableTimer(self):
		self._timerFD.settime(0,0)

	def setIteration(self,iteration):
		self._iteration = iteration
	def getIteration(self):
		return self._iteration
	def setCause(self,cause):
		self._cause = cause
	def getCause(self):
		return self._cause
	def setId(self,id):
		self._id = id
	def getId(self):
		return self._id
	def getPacketType(self):
		return self._packetType
	def setPacketType(self, packetType):
		self._packetType = packetType