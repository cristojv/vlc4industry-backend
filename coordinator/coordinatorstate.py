#import the necessary packages
'''
file: coordinatorstate.py
description:		Implements the class CoordinatorState that can be extend to represent
								a different state of the state machine.
'''
class CoordinatorState():

	def __init__(self,coordinator):
		print("Generating a new state {}".format(str(self)))
		self._coordinator = coordinator
	
	def onEvent(self, event):
		pass
	
	def run(self):
		pass
	
	def __repr__(self):
		return self.__str__()
	
	def	 __str__(self):
		return self.__class__.__name__