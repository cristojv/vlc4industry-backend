from coordinatorsm import CoordinatorSM
from coordinatorevents import BaseEvent, SMEvents
from coordinatorstates import InitState, LaunchingState, ReceivingState, WaitingResponseState
import unittest

class CoordinatorTest(unittest.TestCase):
	
	def setUp(self):
		self.csm = CoordinatorSM()
	
	def test_bestCase(self):
		event = BaseEvent(SMEvents.USER_START)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, LaunchingState.__name__, "Incorrect finish state")
		
		event = BaseEvent(SMEvents.CAMERA_ROIFOUND)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, ReceivingState.__name__, "Incorrect finish state")
		
		event = BaseEvent(SMEvents.USER_COMMAND)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, WaitingResponseState.__name__, "Incorrect finish state")
		
		event = BaseEvent(SMEvents.CAMERA_RESPONSEOK)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, ReceivingState.__name__, "Incorrect finish state")
		
		event = BaseEvent(SMEvents.USER_STOP)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, InitState.__name__, "Incorrect finish state")
			
	def test_CameraResponseError(self):
		events = [
			BaseEvent(SMEvents.USER_START),
			BaseEvent(SMEvents.CAMERA_ROIFOUND),
			BaseEvent(SMEvents.USER_COMMAND),
		]
		
		for event in events:
			self.csm.onEvent(event)
		
		
		event = BaseEvent(SMEvents.CAMERA_RESPONSEERROR)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, WaitingResponseState.__name__, "Incorrect finish state")
		self.assertEqual(self.csm._iteration.get(), 1, "Incorrect iteration number")
		self.assertEqual(self.csm._cause.get(), "CAMERA_RESPONSEERROR", "Incorrect cause")
		
		event = BaseEvent(SMEvents.CAMERA_RESPONSEERROR)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, WaitingResponseState.__name__, "Incorrect finish state")
		self.assertEqual(self.csm._iteration.get(), 2, "Incorrect iteration number")
		self.assertEqual(self.csm._cause.get(), "CAMERA_RESPONSEERROR", "Incorrect cause")
		
		event = BaseEvent(SMEvents.CAMERA_RESPONSEERROR)
		self.csm.onEvent(event)
		self.assertEqual(self.csm._state, ReceivingState.__name__, "Incorrect finish state")
		self.assertEqual(self.csm._iteration.get(), 2, "Incorrect iteration number")
		self.assertEqual(self.csm._cause.get(), "CAMERA_RESPONSEERROR", "Incorrect cause")

if __name__ == "__main__":
	unittest.main()