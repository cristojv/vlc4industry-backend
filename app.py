# import eventlet
from multiprocessing import Process, SimpleQueue
import linuxfd
import select
import time
# eventlet.monkey_patch(time = False)
from flask import Flask, render_template, request, jsonify
from flask_socketio import SocketIO, emit, disconnect
import os
import json
import ssl

#OCC Receiver
from occcamera.occreceiver import OCCReceiver

from coordinator.coordinatorsm import CoordinatorSM
from events.coordinatorevents import CoordinatorEvents, CoordinatorEvent,CoordinatorUserEvent

from threading import Thread
#Serial import
import serial
from mockcamera.mockcamera import MockCamera

# SECRET_GIT
host = '10.13.30.28'
port = '5000'

user_login_code = "qwerty"
admin_login_code = "asdfg"

def create_app():
     app = Flask(__name__, static_url_path ='',static_folder = 'web/static',template_folder = 'web/templates')
     app.config['SECRET_KEY'] = 'secret!'
     app.config['DEBUG'] = False
     socketio.init_app(app)
     return app

'''****************************************************
Global Variables:
****************************************************'''
'''****************************************************
Create flask app and socketio server:
    The flask app serves the website templates
    and the static files to the user within the route
    '/'.
    
    Async_mode recommended is gevent cause
    allows python coroutines to serve multiple clients.
    However this implimentation only serves one user
    at the moment.
****************************************************'''

# WARNING: Do not change this mode. Eventlet and Gevent does not work well
# with python threads. This system uses a background thread to run the camera
# as the data-receiver. The proposed changes could be to split that program to
# another process an communicate with it using MQTT queues.
# This would be set for a TODO implementation
async_mode = 'threading'

socketio = SocketIO(async_mode = async_mode, logger=True, cors_allowed_origins='*')

app = create_app()

@app.route('/')
def index():
    return render_template('index_client.html')

@app.route('/admin')
def index_admin():
    return render_template('index.html')

@app.route('/demos', methods=['GET'])
def get_demos():
    demosJson = 0
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    json_url = os.path.join(SITE_ROOT, "web/static/data", "demos.json")
    with open(json_url,"r") as read_file:
        demosJson = json.load(read_file)
    return jsonify(demosJson)

@app.route('/savedemos', methods=['POST'])
def save_demos():
    content = request.get_json()
    SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
    json_url = os.path.join(SITE_ROOT, "web/static/data", "demos.json")
    with open(json_url,"w") as write_file:
        json.dump(content, write_file)
        return jsonify(msg="Se han guardado correctamente las demos.")
    return jsonify(msg="No Se ha guardado correctamente.")


'''****************************************************
socketConnections:
    Saves the current session id for every client, so it
    will be possible to stablish direct and personal
    communication between the server and every client.
****************************************************'''

socketConnections = []
adminSocketConnections = []

'''****************************************************
backgroundThread:
    Due to the fact that the socketio server catch the
    main event loop of the program is necesary to
    launch a custom event loop to catch the different
    events that occurs when the program is running.
    
    This backgroundThread will be launched once and
    only once after the first succeed connection happens.
****************************************************'''

backgroundThread = Thread()

'''****************************************************
userQueue:
    Queue used for receiving commands from the user.
****************************************************'''
userQueue = SimpleQueue()
externalQueues =[userQueue]
'''****************************************************
SocketIO Server events
****************************************************'''
cameraIsStarted = False
'''****************************************************
Event: 'connect'
namespace: 'main'
Use:
The server will serve only @max of the connections.
The other incoming connections will be refussed.
A event 'connectionRefused' is sent waiting for the
event 'connectionRefusedACK' to desconnect the
client.

In this case @max == 1, so only one connection will
be allowed.
****************************************************'''

def check_code(code):
    global user_login_code
    global admin_login_code

    if code == user_login_code:
        return "user"
    elif code == admin_login_code:
        return "admin"
    else:
        return "none"

refuse_codes = [
    {"id": 0,
     "msg": "Password is wrong"},
    {"id": 1,
     "msg": "The room is full"},
    {"id": 2,
     "msg": "Another admin has been connected"}
]
@socketio.on('connect', namespace='/main')
def mainConnect():
    global backgroundThread
    global externalfd

    code = request.args.get("login_code")
    print("A connection has been received with code: {}, and the \
    password is: {}".format(code, user_login_code))

    client_type = check_code(code)

    print("The client is: {}".format(client_type))
    
    if client_type == 'user':
        if(len(socketConnections)==4):
            # Refuse incomming connection because the room is full.
            connectionRefused(request.sid, refuse_codes[1])
            print("User: {} has been refused".format(request.sid))
        else:
            # Accept incomming connection because there is still space.
            connectionAccepted(request.sid)

        if not backgroundThread.isAlive():
            # Run the background thread
            backgroundThread = EventLoopThread(externalQueues, socketio)
            backgroundThread.start()
            print("User: {} has been connected".format(request.sid))
    
    elif client_type == 'admin':
        
        if(len(adminSocketConnections)==1):
            print(adminSocketConnections[0])
            connectionRefused(adminSocketConnections[0], refuse_codes[2])
            print("Admin: {} has been refused".format(request.sid))
            connectionAdminAccepted(request.sid)
        else:
            connectionAdminAccepted(request.sid)
        if not backgroundThread.isAlive():
            backgroundThread = EventLoopThread(externalQueues, socketio)
            backgroundThread.start()
            print("Admin: {} has been connected".format(request.sid))
    
    else:
        # Refuse not-authenticated connections.
        connectionRefused(request.sid, refuse_codes[0])


@socketio.on('cameraIsStarted', namespace='/main')
def mainConnect(isStarted):
    global cameraIsStarted
    cameraIsStarted = isStarted['isStarted']
    print(isStarted['isStarted'])

'''****************************************************
Event: 'connectionRefusedACK'
namespace: 'main'
Use:
The server will serve only @max of the connections.
The other incoming connections will be refussed. This
is a response for the event 'connectionRefused'. After
receiving this event the client is disconnected.

In this case @max == 1, so only one connection will
be allowed.
****************************************************'''

@socketio.on('connectionRefusedACK', namespace='/main')
def mainConnectionRefused(data):
    if(data['clientsid'] == request.sid):
        disconnect(request.sid)
        print('Client: {} has been disconnected'.format(request.sid));

'''****************************************************
Event: 'disconnect'
namespace: 'main'
Use:
Remove the existing client from the array after a
disconnetion happens
****************************************************'''

@socketio.on('disconnect', namespace='/main')
def mainDisconnect():
    if(socketConnections.count(request.sid)):
        socketConnections.remove(request.sid);
    print("Client: {} has been disconnected".format(request.sid));

'''****************************************************
Event: 'changeUserCode'
namespace: 'main'
Use:
Change the login code for users.
If there is any user in the room they would be disconnected.
****************************************************'''
def disconnect_all_users():
    global socketConnections
    for socket_connection in socketConnections:
        print("Disconnecting user: {}".format(socket_connection))
        disconnect(socket_connection)
    print("Disconnecting all users")

@socketio.on('changeUserCode', namespace='/main')
def change_user_code(msg):
    global user_login_code

    disconnect_all_users()
    print("New code: {}".format(msg["user_code"]))
    user_code = msg["user_code"]
    user_login_code = user_code

'''****************************************************
Event: 'start'
namespace: 'main'
Use:
Init and start the process in charge of the camera
receiver.
****************************************************'''

@socketio.on('start', namespace='/main')
def start():
    global userQueue
    userQueue.put(CoordinatorEvent(CoordinatorEvents.USER_START))

'''****************************************************
Event: 'stop'
namespace: 'main'
Use:
Stops the process in charge of the camera receiver.
****************************************************'''

@socketio.on('stop', namespace='/main')
def stop():
    global userQueue
    userQueue.put(CoordinatorEvent(CoordinatorEvents.USER_STOP))

'''****************************************************
Event: 'command'
namespace: 'main'
Use:
Receive commands from the user.
****************************************************'''

@socketio.on('command', namespace='/main')
def command(command):
    type = command ['type']
    params = command ['params']

    if(type == 'meas'): #TODO change id
        userCommand = "MEAS:{}?".format(params[0])
        packetType = params[0]
    elif(type == 'actionInc'):
        userCommand = "ARM:{}:{}".format(params[0],params[1])
        packetType = 'action'
    elif(type == 'actionAbs'):
        userCommand = "ARM:ABS {} {} {} {} {} {}".format(params[0], params[1], params[2], params[3], params[4], params[5])
        print(userCommand)
        packetType = 'action'
    global userQueue
    userQueue.put(CoordinatorUserEvent(CoordinatorEvents.USER_COMMAND, type=packetType, command = userCommand))

'''****************************************************
SocketIO Server actions
****************************************************'''
'''****************************************************
Function: 'connectionRefused'
namespace: 'main'
Use:
The server sends a event to acknoledge the client
that the connection has been refused.
****************************************************'''

def connectionRefused(clientsid, reason):
    if clientsid in socketConnections:
        socketConnections.remove(clientsid)
    print("Sending refuse reason: {} - {}".format(reason["id"],
                                                  reason["msg"]))
    emit('connectionRefused',{'clientsid': clientsid,
                              'reason_id': reason["id"],
                              'reason_msg': reason["msg"]},
         namespace='/main', room=clientsid)

'''****************************************************
Function: 'connectionAccepted'
namespace: 'main'
Use:
The server sends a event to acknoledge the client
that the connection has been accepted.
****************************************************'''

def connectionAccepted(clientsid):
    socketConnections.append(clientsid)
    socketio.emit('connectionAccepted',{'isStarted':cameraIsStarted}, namespace='/main', room=clientsid)

'''****************************************************
Function: 'connectionAdminAccepted'
namespace: 'main'
Use:
The server sends a event to acknoledge the client
that the connection has been accepted.
****************************************************'''

def connectionAdminAccepted(clientsid):
    global adminSocketConnections
    adminSocketConnections.append(clientsid)
    socketio.emit('connectionAccepted',{'isStarted':cameraIsStarted}, namespace='/main', room=clientsid)

'''****************************************************
Function: 'messageClient'
namespace: 'main'
Use:
The server sends a message to the client to be
shown within the console.
****************************************************'''

def messageClient(clientsid, message):
    if(socketConnections.count(clientsid)):
        socketio.emit('messageClient', {'msg': message}, namespace='/main', room=clientsid)

'''****************************************************
MAIN
****************************************************'''

class EventLoopThread(Thread):
    def __init__(self, externalQueues, userSocketIO):
        super(EventLoopThread, self).__init__()
        self._externalQueues = externalQueues
        self._userSocketIO = userSocketIO
        
    def run(self):
        # Flag variables
        isRunning = False
        
        # Internal Queues
        toCameraQueue = SimpleQueue()
        froCameraQueue = SimpleQueue()
        internalQueues = [froCameraQueue]
        
        #List of file descriptors used for reading.
        queues = self._externalQueues + internalQueues
        dictQueues = {value._reader : value for value in queues}
        fdlist = list(dictQueues.keys())
        
        #Init Arduino Serial
        # CHANGE
        arduinoPort = '/dev/ttyACM0'
        arduinoSerial = None
        try:
            arduinoSerial = serial.Serial('/dev/ttyACM0')
            print("Arduino launched!")
        except serial.SerialException:
            #Arduino has not been launched
            print("Arduino not correct conected. Check Ports")
            self._userSocketIO.emit('errorClient', {'msg': 'La lampara de transmision no esta debidamente conectada. '\
            'El fallo se debe a que el puerto de comunicaciones con la placa controladora no es correcto.'}, namespace='/main')
            return
        
        #Init coordinator
        tfd = linuxfd.timerfd(rtc=True, nonBlocking=True)
        tfd.settime(0,0)
        coordinatorSM = CoordinatorSM(toCameraQueue,self._userSocketIO,arduinoSerial, tfd)
        fdlist.append(tfd.fileno())
        
        #Init Camera process
        # CHANGE
        # occReceiver = MockCamera(toCameraQueue,froCameraQueue,101)
        # occReceiver.start()
        occReceiver = OCCReceiver(toCameraQueue, froCameraQueue)
        occReceiver.start()
        isRunning = True
        
        while (isRunning):
            print("EventLOOP > Waiting for new events")
            read_sockets, write_sockets, error_sockets = select.select(fdlist, [], [])
            print("EventLOOP > New events has reached")
            #Only listening for reading socket
            for sock in read_sockets:
                #TODO switch case between sockets. Now there is only Processing.Queue sockets
                if sock in dictQueues:
                    coordinatorSM.onEvent(dictQueues[sock].get()) #TODO Change to get_nowait()?
                elif(sock == tfd.fileno()):
                    tfd.read();
                    event = CoordinatorEvent(CoordinatorEvents.CONTROLLER_TIMEOUT)
                    coordinatorSM.onEvent(event)
                else:
                    print("Event> {}".format(sock))
                    print("tfd> {}".format(tfd.fileno()))
if __name__ == '__main__':
    
    # context = ssl.SSLContext()
    # context.load_verify_locations('./certs/server.pem', './certs/server.key')
    socketio.run(app,host=(host),port=int(port),debug=True)#,certfile='./certs/server.pem',keyfile='./certs/server.key')#, threaded = True)