#import the necessary packages
from multiprocessing import Process, Queue
from events.cameraevents import CameraEvents, CameraEvent
from events.coordinatorevents import CoordinatorEvents, CoordinatorEvent
from time import sleep
class MockCamera(Process):
	def __init__(self, inputQueue, outputQueue, packetID):
		super(MockCamera, self).__init__()
		self.inputQueue = inputQueue
		self.outputQueue = outputQueue
	def run(self):
		while(True):
			event = self.inputQueue.get()
			print("CAMERA new Event")
			print(type(event))
			print(type(CameraEvent(1)))
			if (isinstance(event, CameraEvent)):
				if(event.getId()==CameraEvents.CAMERA_LAUNCH):
					self.outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROIFOUND))
					print("emited response1")
					sleep(10)
					self.outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_RESPONSEOK))
					print("emited response3")
				
				elif(event.getId()==CameraEvents.CAMERA_CONFIG):
					sleep(10)
					self.outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_NEWFRAME))
					
				elif(event.getId()==CameraEvents.CAMERA_CHECK):
					sleep(10)
					self.outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROIFOUND))
					
				elif(event.getId()==CameraEvents.CAMERA_STOP):
					pass