# Project: Visible Light Communications (VLC) for Industry 4.0
## Block: Python-based backend

### Description:
🖥️ Backend developed using Flask and deployed in a raspberry Pi 3B.

It performs the following routines:
1) 🚥 Coordinates the **download (tx)** link 💡 using the serial port 🔌.
2) 🚥 oordinates the **upload (rx)** link 📷 using the camera and OpenCV 👁️.
3) Serves a static website for user/admin **interface**.
4) 🚥 Coordinates the user/admin instructions using **websockets**.