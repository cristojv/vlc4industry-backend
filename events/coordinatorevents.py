#import the necessary packages

from enum import Enum
from .baseevents import BaseEvent
'''
file: coordinatorevents.py
description: Description of base events for the state machine.
'''
class CoordinatorEvents(Enum):
	USER_START = 1
	USER_CONFIG = 2
	USER_COMMAND = 3
	USER_STOP = 4
	
	CAMERA_ROIFOUND = 10
	CAMERA_ROINOTFOUND  = 11
	CAMERA_ROILOST = 12
	CAMERA_RESPONSEOK = 13
	CAMERA_RESPONSEERROR = 14
	
	CONTROLLER_TIMEOUT = 15

class CoordinatorEvent(BaseEvent):
	pass
	
class CoordinatorCameraEvent(CoordinatorEvent):
        def __init__(self,id,responseid, response, data):
            super(CoordinatorCameraEvent, self).__init__(id)
            self._responseid = responseid
            self._response = response
            self._data = data
        def getResponseId(self):
        	return self._responseid
        def getResponse(self):
        	return self._response
        def getData(self):
        	return self._data
            
class CoordinatorUserEvent(CoordinatorEvent):
	def __init__(self,id,type='',command = ''):
		super(CoordinatorUserEvent, self).__init__(id)
		self._type = type
		self._command = command

	def getType(self):
		return self._type
	def getCommand(self):
		return self._command