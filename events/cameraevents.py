#import the necessary packages

from enum import Enum
from .baseevents import BaseEvent
'''
file: cameraevents.py
description: Description of base events for the state machine.
'''
class CameraEvents(Enum):
	CAMERA_LAUNCH = 1
	CAMERA_CONFIG = 2
	CAMERA_STOP = 3
	CAMERA_CHECK = 4
	CAMERA_NEWFRAME = 5

class CameraEvent(BaseEvent):
	pass