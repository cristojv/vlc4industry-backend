#import the necessary packages

'''
file:           UserEvent.py
description:
                This file implements the event class
                for generating the event object to
                be sent over the communication to acknoledge
                the controller to act.
'''

class UserEvent:
    def __init__(self, id, status, type, args):
        
        self._id = id
        self._type = type
        self._args = args
    
    def getId(self):
        return self._id
    def getStatus(self):
        return self._status
    def getType(self):
        return self._type
    def getData(self):
        return self._data